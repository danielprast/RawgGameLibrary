//
//  GameCollectionResponse.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation


public struct GameResponseModel: Codable {
  
  public let count: Int
  public let next: String
  public let previous: String
  public let games: [GameData]
  
  public enum CodingKeys: String, CodingKey {
    case count, next, previous
    case games = "results"
  }
  
  public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    self.count = try container.decodeIfPresent(Int.self, forKey: .count) ?? 0
    self.next = try container.decodeIfPresent(String.self, forKey: .next) ?? ""
    self.previous = try container.decodeIfPresent(String.self, forKey: .previous) ?? ""
    self.games = try container.decodeIfPresent([GameData].self, forKey: .games) ?? []
  }
  
  // MARK: -
  
  public struct GameData: Codable {
    
    public static func createDefault() -> GameData {
      return GameData(
        id: -1,
        slug: "",
        name: "",
        released: "",
        image: "",
        rating: 0.0
      )
    }
    
    public init(
      id: Int,
      slug: String,
      name: String,
      released: String,
      image: String,
      rating: Double
    ) {
      self.id = id
      self.slug = slug
      self.name = name
      self.released = released
      self.image = image
      self.rating = rating
    }
    
    public let id: Int
    public let slug: String
    public let name: String
    public let released: String
    public let image: String
    public let rating: Double
    
    public enum CodingKeys: String, CodingKey {
      case id, slug, released, name
      case rating
      case image = "background_image"
    }
    
    public init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? -1
      self.slug = try container.decodeIfPresent(String.self, forKey: .slug) ?? ""
      self.released = try container.decodeIfPresent(String.self, forKey: .released) ?? ""
      self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
      self.rating = try container.decodeIfPresent(Double.self, forKey: .rating) ?? 0.0
      self.image = try container.decodeIfPresent(String.self, forKey: .image) ?? ""
    }
    
  }
  
}
