//
//  GameDetailResponseModel.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation


public struct GameDetailResponseModel: Codable {
  
  public let id: Int
  public let slug: String
  public let name: String
  public let image: String
  public let description: String
  public let rating: Double
  public let released: String
  public let developers: [Developer]
  public let status: AddedByStatus
  
  public enum CodingKeys: String, CodingKey {
    case id, slug, name
    case rating
    case released
    case description = "description_raw"
    case developers
    case status = "added_by_status"
    case image = "background_image"
  }
  
  public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? -1
    self.slug = try container.decodeIfPresent(String.self, forKey: .slug) ?? ""
    self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
    self.rating = try container.decodeIfPresent(Double.self, forKey: .rating) ?? 0.0
    self.released = try container.decodeIfPresent(String.self, forKey: .released) ?? ""
    self.description = try container.decodeIfPresent(String.self, forKey: .description) ?? ""
    self.developers = try container.decodeIfPresent([Developer].self, forKey: .developers) ?? []
    self.status = try container.decodeIfPresent(AddedByStatus.self, forKey: .status) ?? .makeDefault()
    self.image = try container.decodeIfPresent(String.self, forKey: .image) ?? ""
  }
  
  // MARK: -
  
  public struct AddedByStatus: Codable {
    
    public init(playing: Int) {
      self.playing = playing
    }
    
    public let playing: Int
    
    public init(from decoder: Decoder) throws {
      let container: KeyedDecodingContainer<GameDetailResponseModel.AddedByStatus.CodingKeys> = try decoder.container(keyedBy: GameDetailResponseModel.AddedByStatus.CodingKeys.self)
      self.playing = try container.decodeIfPresent(Int.self, forKey: GameDetailResponseModel.AddedByStatus.CodingKeys.playing) ?? 0
    }
    
    public static func makeDefault() -> AddedByStatus {
      return AddedByStatus(playing: -1)
    }
  }
  
  // MARK: -
  
  public struct Developer: Codable {
    
    public let name: String
    
    public enum CodingKeys: String, CodingKey {
      case name
    }
    
    public init(from decoder: Decoder) throws {
      let container: KeyedDecodingContainer<GameDetailResponseModel.Developer.CodingKeys> = try decoder.container(keyedBy: GameDetailResponseModel.Developer.CodingKeys.self)
      self.name = try container.decodeIfPresent(String.self, forKey: GameDetailResponseModel.Developer.CodingKeys.name) ?? ""
    }
    
    public init(name: String) {
      self.name = name
    }
  }
  
  
}
