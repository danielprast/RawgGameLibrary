//
//  GameLocalDataSource.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import Combine
import CoreData


public protocol GameLocalDataSource {
  
  func getAllFavoriteGames() -> AnyPublisher<[GameEntity.GameData], NError>
  func readFavoriteGame(game: GameEntity.GameData) -> AnyPublisher<GameEntity.GameData?, NError>
  func add(favoriteGame game: GameEntity.GameData) -> AnyPublisher<Bool, NError>
  func delete(favoriteGame game: GameEntity.GameData) -> AnyPublisher<Bool, NError>
  
}


public struct GameCoreDataLocalSource: GameLocalDataSource {
  
  let moc: NSManagedObjectContext
  
  public init(moc: NSManagedObjectContext) {
    self.moc = moc
  }
  
  public func getAllFavoriteGames() -> AnyPublisher<[GameEntity.GameData], NError> {
    return Future<[GameEntity.GameData], NError> { promise in
      let fetchRequest = FavoriteGame.fetchRequest()
      do {
        let favorites = try moc.fetch(fetchRequest)
        let entities = favorites.map {
          GameEntity.GameData(
            id: Int($0.id),
            title: $0.title ?? "",
            image: $0.image ?? "",
            rating: $0.rating,
            date: $0.date ?? ""
          )
        }
        promise(.success(entities))
      } catch {
        let nserror = error as NSError
        promise(.failure(NError.custom(nserror.localizedDescription)))
      }
    }.eraseToAnyPublisher()
  }
  
  public func readFavoriteGame(game: GameEntity.GameData) -> AnyPublisher<GameEntity.GameData?, NError> {
    return Future<GameEntity.GameData?, NError> {
      promise in
      
      let fetchRequest = FavoriteGame.fetchRequest()
      fetchRequest.predicate = NSPredicate(format: "id == %i", game.id)
      
      do {
        if let _ = try moc.fetch(fetchRequest).first {
          promise(.success(game))
          return
        }
        promise(.success(nil))
      } catch {
        promise(.failure(NError.custom(error.localizedDescription)))
      }
    }
    .eraseToAnyPublisher()
  }
  
  public func add(favoriteGame game: GameEntity.GameData) -> AnyPublisher<Bool, NError> {
    return Future<Bool, NError> { promise in
      
      let fetchRequest = FavoriteGame.fetchRequest()
      fetchRequest.predicate = NSPredicate(format: "id == %i", game.id)
      
      do {
        if let _ = try moc.fetch(fetchRequest).first {
          promise(.failure(NError.custom("Already saved")))
        } else {
          let favorite = FavoriteGame(context: moc)
          favorite.id = Int16(game.id)
          favorite.title = game.title
          favorite.image = game.image
          favorite.date = game.date
          favorite.rating = game.rating          
          try moc.save()
          promise(.success(true))
        }
      } catch {
        promise(.failure(NError.custom(error.localizedDescription)))
      }
    }
    .eraseToAnyPublisher()
  }
  
  
  public func delete(favoriteGame game: GameEntity.GameData) -> AnyPublisher<Bool, NError> {
    return Future<Bool, NError> { promise in
      let fetchRequest = FavoriteGame.fetchRequest()
      fetchRequest.predicate = NSPredicate(format: "id == %i", game.id)
      
      do {
        if let favorite = try moc.fetch(fetchRequest).first {
          moc.delete(favorite)
          try moc.save()
          promise(.success(true))
        } else {
          promise(.failure(NError.custom("Favorite Game \(game.title) not found!")))
        }
      } catch {
        let nserror = error as NSError
        promise(.failure(NError.custom(nserror.localizedDescription)))
      }
      
    }.eraseToAnyPublisher()
  }
  
}
