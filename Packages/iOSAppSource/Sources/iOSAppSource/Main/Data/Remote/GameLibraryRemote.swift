//
//  GameLibraryRemote.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import Combine


public protocol GameLibraryRemote {
  func fetchGame(page: Int, limit: Int, keyword: String) -> AnyPublisher<GameResponseModel, NError>
}


public protocol GameDetailRemote {
  func fetchGameDetail(id: Int) -> AnyPublisher<GameDetailResponseModel, NError>
}


public struct GameLibraryRemoteImpl {
  
  public  init(networkManager: NetworkManagerLogic) {
    self.networkManager = networkManager
  }
  
  private let networkManager: NetworkManagerLogic
  
  // MARK: ⌘ GameListRemote
  
  public func fetchGame(
    page: Int,
    limit: Int,
    keyword: String
  ) -> AnyPublisher<GameResponseModel, NError> {
    var queries: [String: String] = [
      "key": RemoteConfig.API_KEY,
      "page": "\(page)",
      "page_size" : "\(limit)"
    ]
    
    if !keyword.isEmpty {
      queries["search"] = keyword
    }
    
    let urlRemote = RemoteConfig.makeUrl(
      path: "games",
      queryItems: queries
    )
    shout("game collection url", value: urlRemote?.absoluteString as Any)
    
    guard let url = urlRemote else {
      return Future<GameResponseModel, NError> { promise in
        promise(.failure(.custom("Unknown URL: \(urlRemote?.absoluteString ?? "")")))
      }.eraseToAnyPublisher()
    }
    
    let urlRequest = URLRequest(url: url)
    
    return networkManager.getRequest(of: GameResponseModel.self, url: urlRequest)
      .mapError{ NError.custom($0.localizedDescription) }
      .eraseToAnyPublisher()
  }
  
  // MARK: ⌘ GameDetailRemote
  
  public func fetchGameDetail(id: Int) -> AnyPublisher<GameDetailResponseModel, NError> {
    let queries: [String: String] = [
      "key": RemoteConfig.API_KEY
    ]
    
    let urlRemote = RemoteConfig.makeUrl(
      path: "games/\(id)",
      queryItems: queries
    )
    shout("game detail url", value: urlRemote?.absoluteString as Any)
    
    guard let url = urlRemote else {
      return Future<GameDetailResponseModel, NError> { promise in
        promise(.failure(.custom("Unknown URL: \(urlRemote?.absoluteString ?? "")")))
      }.eraseToAnyPublisher()
    }
    
    let urlRequest = URLRequest(url: url)
    
    return networkManager.getRequest(of: GameDetailResponseModel.self, url: urlRequest)
      .mapError{ NError.custom($0.localizedDescription) }
      .eraseToAnyPublisher()
  }
  
}


extension GameLibraryRemoteImpl: GameLibraryRemote, GameDetailRemote {}
