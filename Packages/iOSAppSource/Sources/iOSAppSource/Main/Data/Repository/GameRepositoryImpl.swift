//
//  GameRepositoryImpl.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import Combine


public typealias GameCollectionResult = AnyPublisher<GameEntity, NError>
public typealias GameCollectionFuture = Future<GameEntity, NError>

public typealias GameDetailFuture = Future<GameDetailEntity, NError>
public typealias GameDetailResult = AnyPublisher<GameDetailEntity, NError>


public struct GameRepositoryImpl: GameRepository {
  
  public init(
    gameLocalDataSource: GameLocalDataSource,
    gameLibraryRemote: GameLibraryRemote,
    gameDetailRemote: GameDetailRemote,
    networkChecker: NetworkConnectionChecker
  ) {
    self.gameLocalDataSource = gameLocalDataSource
    self.gameLibraryRemote = gameLibraryRemote
    self.gameDetailRemote = gameDetailRemote
    self.networkChecker = networkChecker
  }

  let gameLocalDataSource: GameLocalDataSource
  let gameLibraryRemote: GameLibraryRemote
  let gameDetailRemote: GameDetailRemote
  let networkChecker: NetworkConnectionChecker

  public func getGameCollection(page: Int, keyword: String) -> AnyPublisher<GameEntity, NError> {
    func emitConnectionProblemPublisher() -> GameCollectionResult {
      return GameCollectionFuture { promise in
        return promise(.failure(.connectionProblem))
      }.eraseToAnyPublisher()
    }
    
    func emitRemoteFetch() -> GameCollectionResult {
      return gameLibraryRemote.fetchGame(page: page, limit: 10, keyword: keyword)
        .flatMap { response -> GameCollectionResult in
          if response.games.isEmpty {
            return GameCollectionFuture { promise in
              promise(.failure(.emptyResult))
            }.eraseToAnyPublisher()
          }
          let entity = GameEntity.mapFromResponse(response)
          return Just(entity)
            .setFailureType(to: NError.self)
            .eraseToAnyPublisher()
        }.eraseToAnyPublisher()
    }
    
    return Just(networkChecker.isConnected)
      .setFailureType(to: NError.self)
      .eraseToAnyPublisher()
      .flatMap { isConnected -> GameCollectionResult in
        if !isConnected {
          return emitConnectionProblemPublisher()
        }
        return emitRemoteFetch()
      }
      .eraseToAnyPublisher()
  }
  
  public func getGameDetail(id: Int) -> AnyPublisher<GameDetailEntity, NError> {
    func emitConnectionProblemPublisher() -> GameDetailResult {
      return GameDetailFuture { promise in
        return promise(.failure(.connectionProblem))
      }.eraseToAnyPublisher()
    }
    
    func emitRemoteFetch() -> GameDetailResult {
      return gameDetailRemote.fetchGameDetail(id: id)
        .flatMap { response -> GameDetailResult in
          let entity = GameDetailEntity.mapFromResponse(model: response)
          return Just(entity)
            .setFailureType(to: NError.self)
            .eraseToAnyPublisher()
        }.eraseToAnyPublisher()
    }
    
    return Just(networkChecker.isConnected)
      .setFailureType(to: NError.self)
      .eraseToAnyPublisher()
      .flatMap { isConnected -> GameDetailResult in
        if !isConnected {
          return emitConnectionProblemPublisher()
        }
        return emitRemoteFetch()
      }
      .eraseToAnyPublisher()
  }

  public func getFavoriteGame() -> AnyPublisher<[GameEntity.GameData], NError> {
    return gameLocalDataSource.getAllFavoriteGames()
  }
  
  public func readFavoriteGame(game: GameEntity.GameData) -> AnyPublisher<GameEntity.GameData?, NError> {
    return gameLocalDataSource.readFavoriteGame(game: game)
  }

  public func addFavoriteGame(game: GameEntity.GameData) -> AnyPublisher<Bool, NError> {
    return gameLocalDataSource.add(favoriteGame: game)
  }

  public func deleteFavoriteGame(game: GameEntity.GameData) -> AnyPublisher<Bool, NError> {
    return gameLocalDataSource.delete(favoriteGame: game)
  }

}
