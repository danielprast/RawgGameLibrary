//
//  FakeGameRepository.swift
//  Created by Daniel Prastiwa on 10/08/23.
//

import Foundation
import Combine


//public struct FakeGameRepository: GameRepository {
//  
//  public func getGameCollection(page: Int, keyword: String) -> AnyPublisher<GameEntity, NError> {
//    <#code#>
//  }
//  
//  public func getGameDetail(id: Int) -> AnyPublisher<GameDetailEntity, NError> {
//    <#code#>
//  }
//  
//  public func getFavoriteGame() -> AnyPublisher<[GameEntity.GameData], NError> {
//    <#code#>
//  }
//  
//  public func addFavoriteGame(game: GameEntity.GameData) -> AnyPublisher<Bool, NError> {
//    <#code#>
//  }
//  
//  public func deleteFavoriteGame(game: GameEntity.GameData) -> AnyPublisher<Bool, NError> {
//    <#code#>
//  }
//  
//}
