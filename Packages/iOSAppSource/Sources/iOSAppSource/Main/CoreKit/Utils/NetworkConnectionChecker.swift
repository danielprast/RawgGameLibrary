//
//  NetworkConnectionChecker.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import SystemConfiguration


public protocol NetworkConnectionChecker {
  var isConnected: Bool { get }
}


public struct FakeNetworkConnectionChecker: NetworkConnectionChecker {
  
  var isNetworkDidConnect: Bool = false
  
  public init(isConnected: Bool = true) {
    isNetworkDidConnect = isConnected
  }
  
  public var isConnected: Bool {
    return isNetworkDidConnect
  }
}


public struct NetworkConnectionCheckerImpl: NetworkConnectionChecker {
  
  public var isConnected: Bool {
    get {
      return isInternetAvailable()
    }
  }
  
  private func isInternetAvailable() -> Bool {
    
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(
      to: &zeroAddress, {
        $0.withMemoryRebound(
          to: sockaddr.self,
          capacity: 1
        ) {
          SCNetworkReachabilityCreateWithAddress(nil, $0)
        }
      }
    ) else {
      return false
    }
    
    var flags: SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
      return false
    }
    
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    
    return (isReachable && !needsConnection)
  }
}
