//
//  NetworkManager.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import Combine


public protocol NetworkManagerLogic {
  func getRequest<T>(of: T.Type, url: URLRequest) -> AnyPublisher<T, Error> where T: Codable
}


public class NetworkManager: NetworkManagerLogic {
  
  public static let shared = NetworkManager()
  
  private init() {}
  
  public func getRequest<T>(of: T.Type, url: URLRequest) -> AnyPublisher<T, Error> where T: Codable {
    
    let urlSession = URLSession(configuration: .default)
    
    return urlSession.dataTaskPublisher(for: url)
      .tryMap { (data, response) in
        guard let httpResponse = response as? HTTPURLResponse,
              httpResponse.statusCode == 200 else {
          
          throw URLError(.badServerResponse)
        }
        
        return data
      }
      .decode(type: T.self, decoder: JSONDecoder())
      .eraseToAnyPublisher()
  }
  
}
