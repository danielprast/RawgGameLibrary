//
//  JsonMapper.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation


public class JsonMapper {
  
  let encoder = JSONEncoder()
  let decoder = JSONDecoder()
  
  public static let shared = JsonMapper()
  private init() {}
  
  public func decodeJson<T: Decodable>(from data: Data) -> T? {
    do {
      return try decoder.decode(T.self, from: data)
    } catch {
      return nil
    }
  }
  
  public func decodeJsonArray<T: Decodable>(from data: Data) -> [T]? {
    do {
      return try decoder.decode([T].self, from: data)
    } catch {
      return nil
    }
  }
  
  public func encode(params: [String: Any]) -> String {
    guard
      let jsonData = try? JSONSerialization.data(
        withJSONObject: params,
        options: .prettyPrinted
      ),
      let jsonString = String(data: jsonData, encoding: .utf8)
    else {
      return ""
    }
    return jsonString
  }
  
}
