//
//  ImageViewInterop.swift
//  Created by Daniel Prastiwa on 10/08/23.
//

import Foundation
import SwiftUI
import UIKit


public struct ImageViewInterop: UIViewRepresentable {
  
  public typealias UIViewType = CustomImageView
  
  var imageUrl: String?
  
  public func makeUIView(context: Context) -> CustomImageView {
    let customImageView = CustomImageView()
    //customImageView.frame = .init(x: 0, y: 0, width: 250, height: 150)
    customImageView.backgroundColor = .blue
    customImageView.loadImageFromURL(imageUrl ?? "")
    return customImageView
  }
  
  public func updateUIView(_ uiView: CustomImageView, context: Context) {
    uiView.translatesAutoresizingMaskIntoConstraints = false
    uiView.widthAnchor.constraint(equalToConstant: 250).isActive = true
    uiView.heightAnchor.constraint(equalToConstant: 150).isActive = true
  }
  
}
