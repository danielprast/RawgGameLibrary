//
//  UIColor+Extensions.swift.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import UIKit


public struct AppColorSet {
  
  public static let white: UIColor = UIColor(hexString: "#FFFFFF")
  public static let black: UIColor = UIColor(hexString: "#000000")
  public static let mainColor: UIColor = UIColor(hexString: "#222B39")
  
}


extension UIColor {
  
  convenience init(hexString: String) {
    var hexFormatted: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
    
    if hexFormatted.hasPrefix("#") {
      hexFormatted = String(hexFormatted.dropFirst())
    }
    
    var rgbValue: UInt64 = 0
    Scanner(string: hexFormatted).scanHexInt64(&rgbValue)
    
    self.init(
      red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
      green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
      blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
      alpha: 1)
  }
  
}
