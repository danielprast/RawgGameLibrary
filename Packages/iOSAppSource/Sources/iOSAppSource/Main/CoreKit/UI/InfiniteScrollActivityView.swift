//
//  InfiniteScrollActivityView.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import UIKit


public class InfiniteScrollActivityView: UIView {
  
  var activityIndicatorView: UIActivityIndicatorView = UIActivityIndicatorView()
  
  static let defaultHeight:CGFloat = 60.0
  
  public required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setupActivityIndicator()
  }
  
  public override init(frame aRect: CGRect) {
    super.init(frame: aRect)
    setupActivityIndicator()
  }
  
  public override func layoutSubviews() {
    super.layoutSubviews()
    let xValue = self.bounds.size.width/2
    activityIndicatorView.center = CGPoint(x: xValue, y: self.bounds.size.height/2)
    print("activity indicator view frame: \(xValue)")
  }
  
  public func setupActivityIndicator() {
    activityIndicatorView.style = .medium
    activityIndicatorView.hidesWhenStopped = true
    self.addSubview(activityIndicatorView)
  }
  
  public func stopAnimating() {
    self.activityIndicatorView.stopAnimating()
    self.isHidden = true
  }
  
  public func startAnimating() {
    self.isHidden = false
    self.activityIndicatorView.startAnimating()
  }
}
