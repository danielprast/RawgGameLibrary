//
//  UIView+Extensions.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import UIKit


extension UIView {
  
  public func fillSuperView(margin: CGFloat = 0) {
    self.translatesAutoresizingMaskIntoConstraints = false
    guard let superview = self.superview else { return }
    self.topAnchor.constraint(equalTo: superview.topAnchor, constant: margin).isActive = true
    self.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: -margin).isActive = true
    self.leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: margin).isActive = true
    self.trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: -margin).isActive = true
  }
  
}


extension UIViewController {
  
  public func loadNibName<T>() -> T? where T: UIView{
    let nib = Bundle.module.loadNibNamed(String(describing: T.self), owner: self)
    let nibView = nib?.first as? T
    return nibView
  }
  
}
