//
//  CoreDataStack.swift
//  Created by Daniel Prastiwa on 08/08/23.
//

import Foundation
import CoreData


open class CoreDataStack {
  
  public static let modelName = "GameLibrary"
  
  public init() {}
  
  public static let managedObjectModel: NSManagedObjectModel = {
    let bundle = Bundle.module
    let modelURL = bundle.url(forResource: modelName,withExtension: ".momd")!
    return NSManagedObjectModel(contentsOf: modelURL)!
  }()
  
  public lazy var mainContext: NSManagedObjectContext = {
    return self.persistentContainer.viewContext
  }()
  
  public lazy var persistentContainer: NSPersistentContainer = {
    let container = NSPersistentContainer(
      name: "GameLibrary",
      managedObjectModel: CoreDataStack.managedObjectModel
    )
    container.loadPersistentStores(
      completionHandler: { (storeDescription, error) in
        if let error = error as NSError? {
          fatalError("Unresolved error \(error), \(error.userInfo)")
        }
      }
    )
    return container
  }()
  
  // MARK: - Core Data Saving support
  
  public func saveContext () {
//    let context = persistentContainer.viewContext
//    if context.hasChanges {
//      do {
//        try context.save()
//      } catch {
//        let nserror = error as NSError
//        fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//      }
//    }
    saveContext(mainContext)
  }
  
  public func saveContext(_ context: NSManagedObjectContext) {
    if context != mainContext {
      saveDerivedContext(context)
      return
    }

    context.perform {
      do {
        try context.save()
      } catch let error as NSError {
        fatalError("Unresolved error \(error), \(error.userInfo)")
      }
    }
  }
  
  public func saveDerivedContext(_ context: NSManagedObjectContext) {
    context.perform {
      do {
        try context.save()
      } catch let error as NSError {
        fatalError("Unresolved error \(error), \(error.userInfo)")
      }

      self.saveContext(self.mainContext)
    }
  }
  
}
