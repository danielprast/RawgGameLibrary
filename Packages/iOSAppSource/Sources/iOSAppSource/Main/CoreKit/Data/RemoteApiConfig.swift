//
//  RemoteApiConfig.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation


public enum RemoteConfig {
  
  public static let BASE_URL = "https://api.rawg.io/api/"
  
  public static let API_KEY = "17b077ef71454663b782533ae78d3430"
  
}


extension RemoteConfig {
  
  public static func makeUrl(
    path: String,
    queryItems: [String: String] = [:]
  ) -> URL? {
    var components = URLComponents(string: BASE_URL)!
    components.path = components.path.appending(path)
    if !queryItems.isEmpty {
      components.queryItems = queryItems.map {
        URLQueryItem(name: $0, value: $1)
      }
    }
    return components.url
  }
  
}
