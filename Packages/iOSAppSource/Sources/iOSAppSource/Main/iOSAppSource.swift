public struct iOSAppSource {
    public private(set) var text = "Hello, World!"

    public init() {
    }
}


public func shout(_ key: String, value: Any) {
  print("😎 \(key) -> \(value)")
}
