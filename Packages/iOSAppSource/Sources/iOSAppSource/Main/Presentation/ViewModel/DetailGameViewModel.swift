//
//  DetailGameViewModel.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import SwiftUI
import Combine


public class DetailGameViewModel: ObservableObject {
  
  public init(
    gameRepository: GameRepository,
    game: GameEntity.GameData
  ) {
    self.gameRepository = gameRepository
    gameEntity = game
  }
  
  let gameRepository: GameRepository
  let gameEntity: GameEntity.GameData
  
  var subscriptions = Set<AnyCancellable>()
  var gameDetailEntity: GameDetailEntity = .makeDefault()
  
  @Published var isFavorite: Bool = false
  @Published var unfavoriteError: NError?
  @Published var getDetailError: NError?
  @Published var gameViewModel: GameViewModel?
  @Published var isLoading: Bool = false
  @Published var favoriteProgress: Bool = false
  @Published var gameTitle: String = ""
  
  var developerName: String {
    gameDetailEntity.developerName
  }
  
  var descriptionText: String {
    gameDetailEntity.description
  }
  
  var imageURL: String {
    gameDetailEntity.image
  }
  
  var ratingStr: String {
    return "⭐️\(gameDetailEntity.rating)"
  }
  
  var played: String {
    return "🎮 \(gameDetailEntity.playedCount) played"
  }
  
  var releaseDate: String {
    return "Release date \(gameDetailEntity.released)"
  }
  
  @Published var bannerImage: UIImage?
  
  public func loadImageFromURL() {
    if imageURL.isEmpty {
      return
    }
    
    let url = URL(string: imageURL)
    
    if let url = url {
      
      URLSession.shared.dataTask(with: url) { (data, response, error) in
        if error != nil {
          print(String(describing: error))
          return
        }
        
        DispatchQueue.main.async {
          self.bannerImage = UIImage(data: data!)?.downsample(reductionAmount: 0.2)
        }
        
      }.resume()
    }
    
  }
  
  func getGameDetail() {
    showMainProgress()
    gameRepository.getGameDetail(id: gameEntity.id)
      .subscribe(on: DispatchQueue.global(qos: .userInitiated))
      .eraseToAnyPublisher()
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()
      .sink { [weak self] completion in
        self?.hideMainProgress()
        switch completion {
        case .finished:
          break
        case .failure(let error):
          self?.handle(getDetailError: error)
        }
      } receiveValue: { [weak self] gameDetailEntity in
        self?.handleGetGameDetailResult(gameDetailEntity)
      }
      .store(in: &subscriptions)
  }
  
  func getFavoritedGameLibraries() {
    gameRepository.getFavoriteGame()
      .subscribe(on: DispatchQueue.global(qos: .userInitiated))
      .eraseToAnyPublisher()
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()
      .sink { [weak self] completion in
        self?.hideMainProgress()
        switch completion {
        case .finished:
          break
        case .failure(let error):
          self?.handleGetFavoriteGameFailure(error)
        }
      } receiveValue: { [weak self] gameEntities in
        self?.handleGetFavoriteGameResult(gameEntities)
      }
      .store(in: &subscriptions)
    
  }
  
  func checkIsFavoriteGame() {
    gameRepository.readFavoriteGame(game: gameEntity)
      .subscribe(on: DispatchQueue.global(qos: .userInitiated))
      .eraseToAnyPublisher()
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()
      .sink { [weak self] completion in
        
        switch completion {
        case .finished:
          break
        case .failure(let error):
          self?.handleCheckIsFavoriteError(error)
        }
      } receiveValue: { [weak self] game in
        self?.handleCheckIsFavoriteGame(game)
      }
      .store(in: &subscriptions)
  }
  
  func addToFavorite() {
    gameRepository.addFavoriteGame(game: gameEntity)
      .subscribe(on: DispatchQueue.global(qos: .userInitiated))
      .eraseToAnyPublisher()
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()
      .sink { [weak self] completion in
        self?.hideFavoriteProgress()
        switch completion {
        case .finished:
          break
        case .failure(let error):
          self?.handleAddFavoriteError(error)
        }
      } receiveValue: { [weak self] success in
        self?.handleAddFavoriteResult(success)
      }
      .store(in: &subscriptions)
  }
  
  func removeFromFavorite() {
    gameRepository.deleteFavoriteGame(game: gameEntity)
      .subscribe(on: DispatchQueue.global(qos: .userInitiated))
      .eraseToAnyPublisher()
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()
      .sink { [weak self] completion in
        self?.hideFavoriteProgress()
        switch completion {
        case .finished:
          break
        case .failure(let error):
          self?.handleUnfavoriteError(error)
        }
      } receiveValue: { [weak self] success in
        self?.handleUnfavoriteResult(success)
      }
      .store(in: &subscriptions)
  }
  
  func hideMainProgress() {
    isLoading = false
  }
  
  func showMainProgress() {
    isLoading = true
  }
  
  func showFavoriteProgress() {
    favoriteProgress = true
  }
  
  func hideFavoriteProgress() {
    favoriteProgress = false
  }
  
  fileprivate func handleGetGameDetailResult(_ entity: GameDetailEntity) {
    gameDetailEntity = entity
    gameTitle = entity.title
    loadImageFromURL()
  }
  
  fileprivate func handle(getDetailError: NError?) {
    self.getDetailError = getDetailError
  }
  
  fileprivate func handleAddFavoriteError(_ error: NError?) {
    
  }
  
  fileprivate func handleUnfavoriteResult(_ success: Bool) {
    isFavorite = false
    if !success {
      return
    }
  }
  
  fileprivate func handleAddFavoriteResult(_ success: Bool) {
    isFavorite = true
    if !success {
      return
    }
  }
  
  fileprivate func handleUnfavoriteError(_ error: NError) {
    unfavoriteError = error
  }
  
  fileprivate func handleCheckIsFavoriteGame(_ game: GameEntity.GameData?) {
    isFavorite = game != nil
  }
  
  fileprivate func handleCheckIsFavoriteError(_ error: NError?) {
    
  }
  
  fileprivate func handleGetFavoriteGameResult(_ games: [GameEntity.GameData]) {
    
  }
  
  fileprivate func handleGetFavoriteGameFailure(_ error: NError?) {
    
  }
  
}
