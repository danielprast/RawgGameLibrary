//
//  HomeViewModel.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import Combine


public class HomeViewModel {
  
  public init(
    gameRepository: GameRepository
  ) {
    self.gameRepository = gameRepository
  }  
  
  let gameRepository: GameRepository
  
  var subscriptions = Set<AnyCancellable>()
  var games: [GameEntity.GameData] = []
  var filteredGames: [GameEntity.GameData] = []
  var page = 1
  var pageSearch = 1
  var searchText = ""
  var isNextPageAvaialble: Bool = false
  
  @Published var inputTextSearch: String = ""
  @Published var displayGames: [GameEntity.GameData] = []
  @Published var displayGetGameError: NError?
  @Published var displayGetMoreGamesError: NError?
  @Published var getGameProgress: Bool = false
  @Published var getMoreGameProgress: Bool = false
  
  var searchMode: Bool {
    get {
      !searchText.isEmpty
    }
  }
  
  var pageNumber: Int {
    get {
      if searchMode {
        return pageSearch
      }
      return page
    }
  }
  
  var gameSources: [GameEntity.GameData] {
    get {
      if searchMode {
        return filteredGames
      }
      return games
    }
  }
  
  func showProgress() {
    getGameProgress = true
  }
  
  func hideProgress() {
    getGameProgress = false
  }
  
  func nextPage() {
    if searchMode {
      pageSearch += 1
      return
    }
    page += 1
  }
  
  func resetPage() {
    if searchMode {
      pageSearch = 1
      return
    }
    page = 1
  }
  
  func getGameLibraries() {
    resetPage()
    gameRepository.getGameCollection(page: pageNumber, keyword: searchText)
      .subscribe(on: DispatchQueue.global(qos: .userInitiated))
      .eraseToAnyPublisher()
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()
      .sink { [weak self] completion in
        guard let self = self else { return }
        self.hideProgress()
        switch completion {
        case .finished:
          break
        case .failure(let error):
          self.displayGetGameError = error
        }
      } receiveValue: { [weak self] gameEntity in
        self?.handleGetGameLibraries(gameEntity.data)
      }
      .store(in: &subscriptions)
  }
  
  func showLoadMoreProgress() {
    getMoreGameProgress = true
  }
  
  func hideLoadMoreProgress() {
    getMoreGameProgress = false
  }
  
  func getMoreGameLibraries() {
    nextPage()
    gameRepository.getGameCollection(page: pageNumber, keyword: searchText)
      .subscribe(on: DispatchQueue.global(qos: .userInitiated))
      .eraseToAnyPublisher()
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()
      .sink { [weak self] completion in
        guard let self = self else { return }
        self.hideLoadMoreProgress()
        switch completion {
        case .finished:
          break
        case .failure(let error):
          self.displayGetMoreGamesError = error
        }
      } receiveValue: { [weak self] gameEntity in
        self?.handleGetMoreGameLibraries(gameEntity.data)
      }
      .store(in: &subscriptions)
  }
  
  fileprivate func handleGetGameLibraries(_ games: [GameEntity.GameData]) {
    defer {
      displayGames = gameSources
    }
    
    if searchMode {
      self.filteredGames = games
      return
    }
    
    self.games = games
  }
  
  fileprivate func handleGetMoreGameLibraries(_ games: [GameEntity.GameData]) {
    defer {
      displayGames = gameSources
    }
    
    if searchMode {
      filteredGames.append(contentsOf: games)
      return
    }
    
    self.games.append(contentsOf: games)
  }
  
  func searchGame(_ searchText: String) {
    self.searchText = searchText
    
    if searchText.isEmpty {
      return
    }
    
    showProgress()
    getGameLibraries()
  }
  
  func cancelSearch() {
    displayGames = games
    filteredGames.removeAll()
    searchText = ""
  }
  
  fileprivate func mapToViewModel(_ entity: GameEntity) -> [GameViewModel]{
    return entity
      .data
      .map {
      GameViewModel(entity: $0) { [unowned self] item in
        //self.responder.detail(id: id)
        shout("game item tap", value: item)
      }
    }
  }
  
}
