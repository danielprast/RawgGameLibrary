//
//  FavoriteViewModel.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import Combine


public class FavoriteViewModel {
  
  public init(
    gameRepository: GameRepository
  ) {
    self.gameRepository = gameRepository
  }
  
  let gameRepository: GameRepository
  
  var subscriptions = Set<AnyCancellable>()
  var games: [GameEntity.GameData] = []
  
  @Published var displayGames: [GameEntity.GameData] = []
  @Published var displayGetGameError: NError?
  @Published var unfavoriteError: NError?
  @Published var getGameProgress: Bool = false  
  
  func getFavoritedGameLibraries() {
    gameRepository.getFavoriteGame()
      .subscribe(on: DispatchQueue.global(qos: .userInitiated))
      .eraseToAnyPublisher()
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()
      .sink { [weak self] completion in
        self?.getGameProgress = false
        switch completion {
        case .finished:
          break
        case .failure(let error):
          self?.handleGetFavoriteGameFailure(error)
        }
      } receiveValue: { [weak self] gameEntities in
        self?.handleGetFavoriteGameResult(gameEntities)
      }
      .store(in: &subscriptions)

  }
  
  func unfavoriteGame(gameEntity: GameEntity.GameData) {
    gameRepository.deleteFavoriteGame(game: gameEntity)
      .subscribe(on: DispatchQueue.global(qos: .userInitiated))
      .eraseToAnyPublisher()
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()
      .sink { [weak self] completion in
        switch completion {
        case .finished:
          break
        case .failure(let error):
          self?.handleUnfavoriteError(error)
        }
      } receiveValue: { [weak self] success in
        self?.handleUnfavoriteResult(success)
      }
      .store(in: &subscriptions)
  }
  
  fileprivate func handleUnfavoriteResult(_ success: Bool) {
    if !success {
      return
    }
    let currentList = games
    displayGames = currentList
  }
  
  fileprivate func handleUnfavoriteError(_ error: NError) {
    unfavoriteError = error
  }
  
  fileprivate func handleGetFavoriteGameResult(_ games: [GameEntity.GameData]) {
    displayGames = games
  }
  
  fileprivate func handleGetFavoriteGameFailure(_ error: NError?) {
    
  }
}
