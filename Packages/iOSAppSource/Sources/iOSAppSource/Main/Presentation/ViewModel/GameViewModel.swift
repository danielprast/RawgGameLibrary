//
//  GameViewModel.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation


public struct GameViewModel {
  
  public let id: Int
  public let title: String
  public let image: String
  public let rating: Float
  public let date: String
  public let tapAction: (_ item: GameViewModel) -> Void?
  
  public init(
    entity: GameEntity.GameData,
    tapAction: @escaping (_ item: GameViewModel) -> Void
  ) {
    
    self.id = entity.id
    self.title = entity.title
    self.image = entity.image
    self.rating = entity.rating
    self.date = entity.date
    self.tapAction = tapAction
  }
  
  public var releasedDate: String {
    return "Released \(date)"
  }
  
  public var rate: String {
    return "⭐️ \(rating)"
  }
}
