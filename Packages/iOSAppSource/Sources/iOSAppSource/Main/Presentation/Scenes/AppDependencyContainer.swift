//
//  AppDependencyContainer.swift
//  Created by Daniel Prastiwa on 08/08/23.
//

import Foundation


public class AppDependencyContainer {
  
  public let coreDataStack: CoreDataStack
  public let dic: DICProtocol
  
  public init(
    coreDataStack: CoreDataStack,
    dic: DICProtocol
  ) {
    self.coreDataStack = coreDataStack
    self.dic = dic
    setupDependencies()
  }
  
  private func setupDependencies() {
    let networkChecker = NetworkConnectionCheckerImpl() as AnyObject
    dic.register(type: NetworkConnectionChecker.self) { _ in
      return networkChecker
    }
    
    let gameLibraryRemote = GameLibraryRemoteImpl(
      networkManager: NetworkManager.shared
    ) as AnyObject
    dic.register(type: GameLibraryRemote.self) { _ in
      return gameLibraryRemote
    }
    dic.register(type: GameDetailRemote.self) { _ in
      return gameLibraryRemote
    }
    
    let gameLocalDataSource = GameCoreDataLocalSource(moc: coreDataStack.persistentContainer.viewContext) as AnyObject
    dic.register(type: GameLocalDataSource.self) { _ in
      return gameLocalDataSource
    }
    
    let gameLibraryRepository = GameRepositoryImpl(
      gameLocalDataSource: dic.resolve(type: GameLocalDataSource.self)!,
      gameLibraryRemote: dic.resolve(type: GameLibraryRemote.self)!,
      gameDetailRemote: dic.resolve(type: GameDetailRemote.self)!,
      networkChecker: dic.resolve(type: NetworkConnectionChecker.self)!
    ) as AnyObject
    dic.register(type: GameRepository.self) { _ in
      return gameLibraryRepository
    }
  }
  
  public func makeMainController() -> MainViewController {
    let repository = dic.resolve(type: GameRepository.self)!
    let mainTabContainer = MainTabDependencyContainer(repository: repository)
    return MainViewController(
      mainTabController: mainTabContainer.makeMainTabController()
    )
  }
  
}
