//
//  MainTabController.swift
//  Created by Daniel Prastiwa on 08/08/23.
//

import Foundation
import UIKit


public class MainTabController: UITabBarController {
  
  public init(
    homeNavController: HomeNavController,
    favoriteNavController: FavoriteNavController
  ) {
    self.homeNavController = homeNavController
    self.favoriteNavController = favoriteNavController
    super.init(nibName: nil, bundle: .module)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  let homeNavController: HomeNavController
  let favoriteNavController: FavoriteNavController
  
  public override func viewDidLoad() {
    super.viewDidLoad()
    
    if #available(iOS 15.0, *) {
      let appearance = UITabBarAppearance()
      //appearance.configureWithOpaqueBackground()
      //appearance.backgroundColor = AppColorSet.white
      tabBar.standardAppearance = appearance
      tabBar.tintColor = AppColorSet.mainColor
      tabBar.scrollEdgeAppearance = tabBar.standardAppearance
    }
    
    setupViewControllers()
  }
  
  fileprivate func setupViewControllers() {
    let homeIconNormal = UIImage(systemName: "house")
    let homeIconSelected = UIImage(systemName: "house.fill")
    homeNavController.tabBarItem.image = homeIconNormal
    homeNavController.tabBarItem.selectedImage = homeIconSelected
    
    let favoriteIconNormal = UIImage(systemName: "heart")
    let favoriteIconSelected = UIImage(systemName: "heart.fill")
    favoriteNavController.tabBarItem.image = favoriteIconNormal
    favoriteNavController.tabBarItem.selectedImage = favoriteIconSelected
    
    viewControllers = [
      homeNavController,
      favoriteNavController
    ]
    
    guard let items = tabBar.items else { return }
    
    items[0].title = "Home"
    items[1].title = "Favorite"
    
    for item in items {
      item.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
    }
  }
  
}
