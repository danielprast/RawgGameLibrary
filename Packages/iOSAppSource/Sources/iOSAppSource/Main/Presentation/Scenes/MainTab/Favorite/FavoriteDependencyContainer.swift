//
//  FavoriteDependencyContainer.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation


public class FavoriteDependencyContainer {
  
  let repository: GameRepository
  
  public init(
    repository: GameRepository
  ) {
    self.repository = repository
  }
  
  public func makeFavoriteNavController() -> FavoriteNavController {
    func makeFavoriteViewModel() -> FavoriteViewModel {
      return FavoriteViewModel(gameRepository: self.repository)
    }
    
    func makeFavoriteController() -> FavoriteController {
      return FavoriteController(viewModel: makeFavoriteViewModel())
    }
    
    let detailGameFactory: (GameEntity.GameData) -> DetailGameController = { gameEntity in
      return self.makeDetailGameController(gameEntity: gameEntity)
    }
    
    return FavoriteNavController(
      rootController: makeFavoriteController(),
      gameDetailControllerFactory: detailGameFactory
    )
  }
  
  public func makeDetailGameController(gameEntity: GameEntity.GameData) -> DetailGameController {
    func makeViewModel() -> DetailGameViewModel {
      return DetailGameViewModel(
        gameRepository: self.repository,
        game: gameEntity
      )
    }
    return DetailGameController(viewModel: makeViewModel())
  }
  
}
