//
//  MainTabDependencyContainer.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation


public class MainTabDependencyContainer {
  
  let repository: GameRepository
  
  public init(
    repository: GameRepository
  ) {
    self.repository = repository
  }
  
  public func makeMainTabController() -> MainTabController {
    let homeContainer = makeHomeDependencyContainer()
    let favoriteContainer = makeFavoriteDependencyContainer()
    return MainTabController(
      homeNavController: homeContainer.makeHomeNavController(),
      favoriteNavController: favoriteContainer.makeFavoriteNavController()
    )
  }
  
  fileprivate func makeHomeDependencyContainer() -> HomeDependencyContainer {
    return HomeDependencyContainer(repository: self.repository)
  }
  
  fileprivate func makeFavoriteDependencyContainer() -> FavoriteDependencyContainer {
    return FavoriteDependencyContainer(repository: self.repository)
  }
  
}
