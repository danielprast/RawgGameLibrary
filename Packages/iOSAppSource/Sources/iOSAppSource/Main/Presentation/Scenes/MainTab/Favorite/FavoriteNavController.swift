//
//  FavoriteNavController.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import UIKit


public class FavoriteNavController: GameLibraryNavigationController {
  
  public init(
    rootController: FavoriteController,
    gameDetailControllerFactory: @escaping (GameEntity.GameData) -> DetailGameController
  ) {
    self.rootController = rootController
    self.makeGameDetailController = gameDetailControllerFactory
    super.init()
  }
  
  let rootController: FavoriteController
  let makeGameDetailController: (GameEntity.GameData) -> DetailGameController
  
  public override func presentRootController() {
    rootController.detailGameFlow = self
    rootController.title = "Favorite Games"
    rootController.navigationItem.largeTitleDisplayMode = .always
    pushViewController(rootController, animated: false)
  }
  
  public override func presentDetail(game: GameEntity.GameData) {
    let controller = makeGameDetailController(game)
    controller.hidesBottomBarWhenPushed = true
    controller.title = "Detail"
    pushViewController(controller, animated: true)
  }
  
}
