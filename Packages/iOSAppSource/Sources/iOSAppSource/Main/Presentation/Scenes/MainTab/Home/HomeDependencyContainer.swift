//
//  HomeDependencyContainer.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation


public class HomeDependencyContainer {
  
  let repository: GameRepository
  
  public init(
    repository: GameRepository
  ) {
    self.repository = repository
  }
  
  public func makeHomeNavController() -> HomeNavController {
    func makeHomeViewModel() -> HomeViewModel {
      return HomeViewModel(gameRepository: self.repository)
    }
    
    func makeHomeController() -> HomeController {
      return HomeController(viewModel: makeHomeViewModel())
    }
    
    let detailGameFactory: (GameEntity.GameData) -> DetailGameController = { gameEntity in
      return self.makeDetailGameController(gameEntity: gameEntity)
    }
    
    return HomeNavController(
      rootController: makeHomeController(),
      gameDetailControllerFactory: detailGameFactory
    )
  }
  
  public func makeDetailGameController(gameEntity: GameEntity.GameData) -> DetailGameController {
    func makeViewModel() -> DetailGameViewModel {
      return DetailGameViewModel(
        gameRepository: self.repository,
        game: gameEntity
      )
    }
    return DetailGameController(viewModel: makeViewModel())
  }
  
}
