//
//  HomeNavController.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import UIKit


public class HomeNavController: GameLibraryNavigationController {
  
  public init(
    rootController: HomeController,
    gameDetailControllerFactory: @escaping (GameEntity.GameData) -> DetailGameController
  ) {
    self.rootController = rootController
    self.makeGameDetailController = gameDetailControllerFactory
    super.init()
  }
  
  let rootController: HomeController
  let makeGameDetailController: (GameEntity.GameData) -> DetailGameController
  
  public override func presentRootController() {
    rootController.detailGameFlow = self
    rootController.title = "Games For You"
    rootController.navigationItem.largeTitleDisplayMode = .always
    pushViewController(rootController, animated: false)
  }
  
  public override func presentDetail(game: GameEntity.GameData) {
    let controller = makeGameDetailController(game)
    controller.hidesBottomBarWhenPushed = true
    controller.title = "Detail"
    pushViewController(controller, animated: true)
  }
}
