//
//  MainViewController.swift
//  Created by Daniel Prastiwa on 08/08/23.
//

import Foundation
import UIKit


public class MainViewController: NiblessController {
  
  let mainTabController: MainTabController
  
  public init(mainTabController: MainTabController) {
    self.mainTabController = mainTabController
    super.init()
  }
  
  public override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = AppColorSet.white
    addFullScreen(childViewController: mainTabController)
  }
  
  public override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }
  
}
