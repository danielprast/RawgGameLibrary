//
//  GameLibraryNavigationController.swift
//  Created by Daniel Prastiwa on 10/08/23.
//

import Foundation
import UIKit


public class GameLibraryNavigationController: NiblessNavigationController {
  
  public override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
    presentRootController()
  }
  
  public func setupViews() {
    if #available(iOS 15.0, *) {
      let appearance = UINavigationBarAppearance()
      appearance.configureWithOpaqueBackground()
      appearance.backgroundColor = AppColorSet.mainColor
      appearance.titleTextAttributes = [.foregroundColor: AppColorSet.white]
      appearance.largeTitleTextAttributes = [.foregroundColor: AppColorSet.white]
      navigationBar.tintColor = AppColorSet.white
      navigationBar.standardAppearance = appearance
      navigationBar.scrollEdgeAppearance = appearance
    }
    navigationBar.prefersLargeTitles = true
  }
  
  public func presentRootController() {}
  
  public func presentDetail(game: GameEntity.GameData) {}
  
}


extension GameLibraryNavigationController: DetailGameFlow {}
