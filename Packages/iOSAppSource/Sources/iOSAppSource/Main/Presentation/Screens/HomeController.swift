//
//  HomeController.swift
//  Created by Daniel Prastiwa on 08/08/23.
//

import Foundation
import UIKit
import Combine


public class HomeController: GameListController {
  
  let viewModel: HomeViewModel
  
  public init(viewModel: HomeViewModel) {
    self.viewModel = viewModel
    super.init()
  }
  
  public required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  deinit {
    viewModel.subscriptions.removeAll()
  }
  
  public weak var detailGameFlow: DetailGameFlow?
  
  let searchController: UISearchController = {
    let sc = UISearchController()
    sc.searchBar.placeholder = "Search Games"
    sc.obscuresBackgroundDuringPresentation = false
    return sc
  }()
  
  public override func onViewDidLoad() {
    super.onViewDidLoad()
    
    setupViews()
    
    viewModel.$inputTextSearch
      .debounce(for: .milliseconds(700), scheduler: DispatchQueue.main)
      .sink { [weak self] inputTextSearch in
        guard let self = self else {
          return
        }
        self.viewModel.searchGame(inputTextSearch)
      }
      .store(in: &subscriptions)
    
    viewModel
      .$displayGames
      .sink { [weak self] games in
        self?.handle(displayGames: games)
      }
      .store(in: &subscriptions)
    
    viewModel
      .$displayGetGameError
      .sink { [weak self] error in
        self?.handle(mainError: error)
      }
      .store(in: &subscriptions)
    
    viewModel
      .$displayGetMoreGamesError
      .sink { [weak self] error in
        
      }
      .store(in: &subscriptions)
    
    viewModel
      .$getGameProgress
      .sink { [weak self] progress in
        self?.handle(progress: progress)
      }
      .store(in: &subscriptions)
    
    viewModel
      .$getMoreGameProgress
      .sink { [weak self] progress in
        self?.handle(loadMoreProgress: progress)
      }
      .store(in: &subscriptions)
    
    getInitialData()
  }
  
  public override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if let navController = navigationController {
      navController.navigationBar.prefersLargeTitles = true
    }
  }
  
  public override func getInitialData() {
    viewModel.showProgress()
    viewModel.getGameLibraries()
  }
  
  public override func getMoreData() {
    viewModel.showLoadMoreProgress()
    viewModel.getMoreGameLibraries()
  }
  
  public override func presentGameDetail(_ item: GameEntity.GameData) {
    detailGameFlow?.presentDetail(game: item)
  }
  
  fileprivate func setupViews() {
    searchController.searchResultsUpdater = self
    searchController.searchBar.delegate = self
    navigationItem.searchController = searchController
    
    if let textField = searchController.searchBar.value(forKey: "searchField") as? UITextField {
      textField.backgroundColor = AppColorSet.white
      guard let backgroundView = textField.subviews.first else { return }
      if #available(iOS 11.0, *) {
        backgroundView.backgroundColor = UIColor.white.withAlphaComponent(0.3) //Or any transparent color that matches with the `navigationBar color`
        backgroundView.subviews.forEach({ $0.removeFromSuperview() }) // Fixes an UI bug when searchBar appears or hides when scrolling
      }
      backgroundView.layer.cornerRadius = 10.5
      backgroundView.layer.masksToBounds = true
    }
  }
  
  fileprivate func handle(displayGames: [GameEntity.GameData]) {
    sources = displayGames
    tableView.reloadData()
  }
  
  fileprivate func handle(mainError: NError?) {
    guard let _ = mainError else {
      emptyView.isHidden = true
      return
    }
    emptyView.isHidden = false
  }
  
  fileprivate func handle(loadMoreProgress: Bool) {
    isLoadMoreLoading = loadMoreProgress
    loadingIndicatorView.isHidden = !loadMoreProgress
  }
  
  fileprivate func handle(progress: Bool) {
    isLoading = progress
    tableView.isHidden = progress
    if progress {
      showMainProgress()
      return
    }
    hideMainProgress()
  }
  
  // MARK: ⌘ UISearchBar Delegate
  
  public func updateSearchResults(for searchController: UISearchController) {
    guard let searchText = searchController.searchBar.text else { return }
    viewModel.inputTextSearch = searchText
  }
  
  public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {}
  
  public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    viewModel.cancelSearch()
  }
  
}


extension HomeController: UISearchResultsUpdating,
                          UISearchControllerDelegate,
                          UISearchBarDelegate {}
