//
//  FavoriteController.swift
//  Created by Daniel Prastiwa on 08/08/23.
//

import Foundation
import UIKit


public class FavoriteController: GameListController {
  
  public init(viewModel: FavoriteViewModel) {
    self.viewModel = viewModel
    super.init()
  }
  
  let viewModel: FavoriteViewModel
  public weak var detailGameFlow: DetailGameFlow?
  
  public override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    getInitialData()
  }
  
  public override func onViewDidLoad() {
    super.onViewDidLoad()
    
    isLoadMoreLoading = false
    loadingIndicatorView.removeFromSuperview()
    
    viewModel
      .$displayGames
      .sink { [weak self] games in
        self?.handle(displayGames: games)
      }
      .store(in: &subscriptions)
    
    viewModel
      .$displayGetGameError
      .sink { [weak self] error in
        self?.handle(mainError: error)
      }
      .store(in: &subscriptions)
    
    viewModel
      .$getGameProgress
      .sink { [weak self] progress in
        self?.handle(progress: progress)
      }
      .store(in: &subscriptions)
  }
  
  public override func getInitialData() {
    viewModel.getGameProgress = true
    viewModel.getFavoritedGameLibraries()
  }
  
  public override func getMoreData() {
    
  }
  
  public override func presentGameDetail(_ item: GameEntity.GameData) {
    detailGameFlow?.presentDetail(game: item)
  }
  
  fileprivate func handle(displayGames: [GameEntity.GameData]) {
    sources = displayGames
    tableView.reloadData()
    emptyView.isHidden = !sources.isEmpty
  }
  
  fileprivate func handle(progress: Bool) {
    isLoading = progress
    tableView.isHidden = progress
    if progress {
      showMainProgress()
      return
    }
    hideMainProgress()
  }
  
  fileprivate func handle(mainError: NError?) {
    guard let _ = mainError else {
      emptyView.isHidden = true
      return
    }
    emptyView.isHidden = false
  }
  
}
