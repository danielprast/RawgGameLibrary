//
//  DetailGameController.swift
//  Created by Daniel Prastiwa on 08/08/23.
//

import Foundation
import UIKit
import SwiftUI
import Combine


public class DetailGameController: NiblessController {
  
  let viewModel: DetailGameViewModel
  
  public init(viewModel: DetailGameViewModel) {
    self.viewModel = viewModel
    super.init()
  }
  
  var subscriptions = Set<AnyCancellable>()
  
  lazy var favoriteButton: UIButton = {
    let favImage = UIImage(systemName: "heart")?.withTintColor(AppColorSet.white, renderingMode: .alwaysTemplate)
    let button = UIButton(frame: .zero)
    button.setImage(favImage, for: .normal)
    button.addTarget(
      self,
      action: #selector(favoriteButtonTapped(_:)),
      for: .touchUpInside
    )
    return button
  }()
  
  public override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
    
    viewModel
      .$isFavorite
      .sink { [weak self] isFavorite in
        self?.handle(isFavorite: isFavorite)
      }
      .store(in: &subscriptions)
    
    viewModel.checkIsFavoriteGame()
  }
  
  public override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if let navController = navigationController {
      navController.navigationBar.prefersLargeTitles = false
    }
  }
  
  fileprivate func getInitialData() {
    viewModel.showMainProgress()
    viewModel.getGameDetail()
  }
  
  fileprivate func setupViews() {
    navigationItem.rightBarButtonItem = UIBarButtonItem(customView: favoriteButton)
    
    view.backgroundColor = AppColorSet.white
    
    let hostingController = UIHostingController(rootView: DetailScreenView(viewModel: self.viewModel))
    addFullScreen(childViewController: hostingController)
  }
  
  fileprivate func makeImage(systemName: String = "heart") -> UIImage? {
    return UIImage(systemName: systemName)?
      .withTintColor(
        AppColorSet.white,
        renderingMode: .alwaysTemplate
      )
  }
  
  fileprivate func handle(isFavorite: Bool) {
    let image = isFavorite ? makeImage(systemName: "heart.fill") : makeImage(systemName: "heart")
    favoriteButton.setImage(image, for: .normal)
  }
  
  // MARK: ⌘ Action
  
  @objc fileprivate func favoriteButtonTapped(_ sender: Any) {
    if viewModel.favoriteProgress {
      return
    }
    viewModel.showFavoriteProgress()
    if viewModel.isFavorite {
      viewModel.removeFromFavorite()
      return
    }
    viewModel.addToFavorite()
  }
  
}


// MARK: - ⌘ SwiftUI


public struct DetailScreenView: View {
  
  @ObservedObject public var viewModel: DetailGameViewModel
  
  public init(viewModel: DetailGameViewModel) {
    self.viewModel = viewModel
  }
  
  public var body: some View {
    
    contentView()
      .onAppear {
        viewModel.getGameDetail()
      }
    
  }
  
  @ViewBuilder
  func contentView() -> some View {
    if viewModel.isLoading {
      ZStack {
        Color.black.opacity(0.25)
        ProgressView()
      }.edgesIgnoringSafeArea(.all)
    } else {
      DetailScreenBody(
        titleText: viewModel.gameTitle,
        developerName: viewModel.developerName,
        coverImage: viewModel.bannerImage ?? UIImage(named: "placeholder_cover", in: .module, compatibleWith: .none)!,
        descriptions: viewModel.descriptionText,
        releaseDate: viewModel.releaseDate,
        rating: viewModel.ratingStr,
        playCount: viewModel.played
      )
      
    }
  }
  
}


struct DetailScreenBody: View {
  
  let titleText: String
  let developerName: String
  let coverImage: UIImage
  let descriptions: String
  let releaseDate: String
  let rating: String
  let playCount: String
  
  var body: some View {
    
    ScrollView(showsIndicators: false) {
      
      VStack(spacing: 16) {
        
        Image(uiImage: coverImage)
          .resizable()
          .aspectRatio(contentMode: .fill)
          .frame(width: screen.width, height: 250)
          .clipped()
          .shadow(color: Color.black.opacity(0.3), radius: 10, x: 5, y: 5)
        
        VStack(alignment: .leading, spacing: 16) {
          Text(developerName)
            .font(.system(size: 14, weight: .medium))
            .foregroundColor(Color.gray)
          
          Text(titleText)
            .font(.title)
            .shadow(color: Color.black.opacity(0.3), radius: 10, x: 5, y: 5)
          
          Text(releaseDate)
            .font(.system(size: 14, weight: .medium))
            .foregroundColor(Color.gray)
          
          HStack(spacing: 16) {
            Text(rating)
              .font(.system(size: 12, weight: .medium))
            Text(playCount)
              .font(.system(size: 12, weight: .medium))
          }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding(.horizontal, 16)
        
        Text(descriptions)
          .padding(.horizontal, 16)
          .padding(.top, 16)
          .shadow(color: Color.black.opacity(0.3), radius: 10, x: 5, y: 5)
        
      }
      
    }
    
  }
  
}



struct Previews_DetailGameController_Previews: PreviewProvider {
  
  static var previews: some View {
    DetailScreenBody(
      titleText: "viewModel.gameTitle",
      developerName: "viewModel.developerName",
      coverImage: UIImage(systemName: "person")!,
      descriptions: "viewModel.descriptionText",
      releaseDate: "viewModel.releaseDate",
      rating: "viewModel.ratingStr",
      playCount: "viewModel.playe"
    )
  }
  
}




// MARK: -

public class DetailContentStore: ObservableObject {
  
  @Published var titleText = ""
  
  public init() {}
  
}


public struct DetailScreen_Content_JOSS: View {
  
  @ObservedObject public var dataStore: DetailContentStore
  
  public init(dataStore: DetailContentStore) {
    self.dataStore = dataStore
  }
  
  public var body: some View {
    Text("Juoss Gandoss = \(dataStore.titleText)")
      .onAppear {
        dataStore.titleText = "Mantab Jiwa!!!!"
      }
  }
  
}
