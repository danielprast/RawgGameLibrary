//
//  GameListController.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import UIKit
import Combine


open class GameListController: NiblessController {
  
  var sources: [GameEntity.GameData] = []
  var isLoading = false
  var isLoadMoreLoading = false
  var subscriptions = Set<AnyCancellable>()
  
  var loadingIndicatorView: InfiniteScrollActivityView!
  
  lazy var activityIndicatorView: UIActivityIndicatorView = {
    let indicator = UIActivityIndicatorView(frame: .zero)
    indicator.translatesAutoresizingMaskIntoConstraints = false
    indicator.style = .medium
    indicator.center = view.center
    indicator.tintColor = AppColorSet.mainColor
    return indicator
  }()
  
  lazy var tableView: UITableView = {
    let tv = UITableView(frame: .zero, style: .plain)
    tv.delegate = self
    tv.dataSource = self
    tv.alwaysBounceVertical = true
    tv.register(UITableViewCell.self, forCellReuseIdentifier: "default_cell")
    tv.register(GameTableViewCell.nib, forCellReuseIdentifier: GameTableViewCell.identifier)
    return tv
  }()
  
  lazy var emptyView: EmptyView = loadNibName()!
  
  open override func viewDidLoad() {
    super.viewDidLoad()
    onViewDidLoad()
  }
  
  open func onViewDidLoad() {
    view.backgroundColor = AppColorSet.white
    
    let frame = CGRect(x: 0, y: tableView.contentSize.height, width: tableView.bounds.size.width, height: InfiniteScrollActivityView.defaultHeight)
    loadingIndicatorView = InfiniteScrollActivityView(frame: frame)
    loadingIndicatorView!.isHidden = true
    tableView.addSubview(loadingIndicatorView!)
    
    view.addSubview(tableView)
    view.addSubview(activityIndicatorView)
    view.addSubview(emptyView)
    
    tableView.fillSuperView()
    emptyView.fillSuperView()
    
    NSLayoutConstraint.activate(
      [
        activityIndicatorView.widthAnchor.constraint(equalToConstant: 20),
        activityIndicatorView.heightAnchor.constraint(equalToConstant: 20),
        activityIndicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        activityIndicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -20)
      ]
    )
    
    var insets = tableView.contentInset
    insets.bottom += InfiniteScrollActivityView.defaultHeight
    tableView.contentInset = insets
    
    shout("tableview frame", value: tableView.frame)
  }
  
  open func stopIndicator() {
    loadingIndicatorView?.stopAnimating()
  }
  
  open func getInitialData() {}
  
  open func getMoreData() {}
  
  func hideMainProgress() {
    self.activityIndicatorView.stopAnimating()
    self.activityIndicatorView.isHidden = true
  }
  
  func showMainProgress() {
    self.activityIndicatorView.isHidden = false
    self.activityIndicatorView.startAnimating()
  }
  
  public func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let currentOffset = scrollView.contentOffset.y
    let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
    
    if maximumOffset - currentOffset <= 5.0 && !isLoading {
      
      let frame = CGRect(x: 0, y: tableView.contentSize.height, width: tableView.bounds.size.width, height: InfiniteScrollActivityView.defaultHeight)
      loadingIndicatorView!.frame = frame
      loadingIndicatorView!.startAnimating()
      
      if !isLoadMoreLoading {
        getMoreData()
      }
    }
  }
  
  public func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return sources.count
  }
  
  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let entity = sources[indexPath.row]
    let item = GameViewModel(entity: entity, tapAction: {_ in})
    let cell = tableView.dequeueReusableCell(withIdentifier: GameTableViewCell.identifier, for: indexPath) as! GameTableViewCell
    cell.titleView.text = item.title
    cell.releaseDateLabel.text = item.releasedDate
    cell.ratingLabel.text = item.rate
    cell.thumbailImageView.loadImageFromURL(item.image)
    return cell
  }
  
  public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let entity = sources[indexPath.row]
    presentGameDetail(entity)
    tableView.deselectRow(at: indexPath, animated: true)
  }
  
  open func presentGameDetail(_ item: GameEntity.GameData) {}
  
}


extension GameListController: UITableViewDelegate, UITableViewDataSource {}
