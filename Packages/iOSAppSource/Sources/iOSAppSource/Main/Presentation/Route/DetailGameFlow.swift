//
//  DetailGameFlow.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation


public protocol DetailGameFlow: AnyObject {
  
  func presentDetail(game: GameEntity.GameData)
  
}
