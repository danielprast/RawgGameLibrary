//
//  GameRepository.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation
import Combine


public protocol GameRepository {
  
  func getGameCollection(page: Int, keyword: String) -> AnyPublisher<GameEntity, NError>
  
  func getGameDetail(id: Int) -> AnyPublisher<GameDetailEntity, NError>
  
  func getFavoriteGame() -> AnyPublisher<[GameEntity.GameData], NError>
  
  func readFavoriteGame(game: GameEntity.GameData) -> AnyPublisher<GameEntity.GameData?, NError>
  
  func addFavoriteGame(game: GameEntity.GameData) -> AnyPublisher<Bool, NError>
  
  func deleteFavoriteGame(game: GameEntity.GameData) -> AnyPublisher<Bool, NError>
  
}
