//
//  GameEntity.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation


public struct GameEntity {
  
  public init(count: Int, data: [GameEntity.GameData]) {
    self.count = count
    self.data = data
  }
  
  public let count: Int
  public let data: [GameData]
  
  public static func mapFromResponse(_ model: GameResponseModel) -> GameEntity {
    let games = model.games.map { GameData.mapFromResponse($0) }
    return GameEntity(
      count: model.count,
      data: games
    )
  }
  
  public struct GameData {
    
    public init(id: Int, title: String, image: String, rating: Float, date: String) {
      self.id = id
      self.title = title
      self.image = image
      self.rating = rating
      self.date = date
    }
    
    public let id: Int
    public let title: String
    public let image: String
    public let rating: Float
    public let date: String
    
    public static func mapFromResponse(_ model: GameResponseModel.GameData) -> GameEntity.GameData {
      return GameEntity.GameData(
        id: model.id,
        title: model.name,
        image: model.image,
        rating: Float(model.rating),
        date: model.released
      )
    }
    
  }
  
}
