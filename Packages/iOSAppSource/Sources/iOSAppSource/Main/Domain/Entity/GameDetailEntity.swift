//
//  GameDetailEntity.swift
//  Created by Daniel Prastiwa on 09/08/23.
//

import Foundation


public struct GameDetailEntity {
  
  public let id: Int
  public let title: String
  public let image: String
  public let developerName: String
  public let description: String
  public let released: String
  public let rating: Float
  public let playedCount: Int
  
  public init(
    id: Int,
    title: String,
    image: String,
    developerName: String,
    description: String,
    released: String,
    rating: Float,
    playedCount: Int
  ) {
    self.id = id
    self.title = title
    self.image = image
    self.developerName = developerName
    self.description = description
    self.released = released
    self.rating = rating
    self.playedCount = playedCount
  }
  
  public static func makeDefault() -> GameDetailEntity {
    return GameDetailEntity(
      id: -1,
      title: "",
      image: "",
      developerName: "",
      description: "",
      released: "",
      rating: 0.0,
      playedCount: 0
    )
  }
  
  public static func mapFromResponse(model: GameDetailResponseModel) -> GameDetailEntity {
    return GameDetailEntity(
      id: model.id,
      title: model.name,
      image: model.image,
      developerName: model.developers.isEmpty ? "" : model.developers[0].name,
      description: model.description,
      released: model.released,
      rating: Float(model.rating),
      playedCount: model.status.playing
    )
  }
  
}
