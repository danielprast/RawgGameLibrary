//
//  GameLibraryRemoteTests.swift
//  Created by Daniel Prastiwa on 10/08/23.
//

import XCTest
import Combine
import iOSAppSource
@testable import iOSAppSource


final class GameLibraryRemoteTests: XCTestCase {
  
  var cancellables = Set<AnyCancellable>()
  var sut: GameLibraryRemote!
  
  override func setUp() {
    super.setUp()
    sut = MockGameLibraryRemote()
  }
  
  func testFetchGameCollectionPage1Return10Records() {
    // given
    var games: [GameResponseModel.GameData] = []
    var firstGameTitle: String?
    
    let expectation = expectation(description: "Fetch 10 games for page 1")
    
    // when
    sut.fetchGame(page: 1, limit: 10, keyword: "")
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()
      .sink { completion in
        switch completion {
        case.finished:
          break
        case.failure(_):
          firstGameTitle = nil
          break
        }
        expectation.fulfill()
      } receiveValue: { responseModel in
        games = responseModel.games
        firstGameTitle = games[0].name
      }
      .store(in: &cancellables)
    
    // then
    waitForExpectations(timeout: 10)
    XCTAssertEqual(games.count, 10)
    XCTAssertNotNil(firstGameTitle)
    XCTAssertEqual(firstGameTitle, "Grand Theft Auto V")
  }
  
  func testFetchGameCollectionPage2Return10Records() {
    // given
    var games: [GameResponseModel.GameData] = []
    var firstGameTitle: String?
    
    let expectation = expectation(description: "Fetch 10 games for page 2")
    
    // when
    sut.fetchGame(page: 2, limit: 10, keyword: "")
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()
      .sink { completion in
        switch completion {
        case.finished:
          break
        case.failure(_):
          firstGameTitle = nil
          break
        }
        expectation.fulfill()
      } receiveValue: { responseModel in
        games = responseModel.games
        firstGameTitle = games[0].name
      }
      .store(in: &cancellables)
    
    // then
    waitForExpectations(timeout: 10)
    XCTAssertEqual(games.count, 10)
    XCTAssertNotNil(firstGameTitle)
    XCTAssertEqual(firstGameTitle, "Borderlands 2")
  }
  
  func testFetchGameCollectionPage3Return10Records() {
    // given
    var games: [GameResponseModel.GameData] = []
    var firstGameTitle: String?
    
    let expectation = expectation(description: "Fetch 10 games for page 3")
    
    // when
    sut.fetchGame(page: 3, limit: 10, keyword: "")
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()
      .sink { completion in
        switch completion {
        case.finished:
          break
        case.failure(_):
          firstGameTitle = nil
          break
        }
        expectation.fulfill()
      } receiveValue: { responseModel in
        games = responseModel.games
        firstGameTitle = games[0].name
      }
      .store(in: &cancellables)
    
    // then
    waitForExpectations(timeout: 10)
    XCTAssertEqual(games.count, 10)
    XCTAssertNotNil(firstGameTitle)
    XCTAssertEqual(firstGameTitle, "PAYDAY 2")
  }
  
}


// MARK: -


struct MockGameLibraryRemote: GameLibraryRemote {
  
  func fetchGame(page: Int, limit: Int, keyword: String) -> AnyPublisher<GameResponseModel, NError> {
    return Future<GameResponseModel, NError> { promise in
      //let jsonData = LocalJsonFileReader.readJsonFile(fileName: "game_collection_page1.json")
      let jsonData = ModuleLocalService().loadJsonFromFile(with: "game_collection_page\(page)")
      let model = ModuleLocalService().parsingJSON(of: GameResponseModel.self, from: jsonData)
      switch model {
      case .failure(let error):
        promise(.failure(error))
        break
      case .success(let data):
        promise(.success(data))
        break
      }
    }.eraseToAnyPublisher()
  }
  
}
