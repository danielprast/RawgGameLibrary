//
//  TestCoreDataStack.swift
//  Created by Daniel Prastiwa on 10/08/23.
//

import CoreData
import iOSAppSource


class TestCoreDataStack: CoreDataStack {
  
  override init() {
    super.init()
    
    let persistentStoreDescription = NSPersistentStoreDescription()
    persistentStoreDescription.type = NSInMemoryStoreType
    
    let container = NSPersistentContainer(
      name: CoreDataStack.modelName,
      managedObjectModel: CoreDataStack.managedObjectModel
    )
    
    container.persistentStoreDescriptions = [persistentStoreDescription]
    
    container.loadPersistentStores { _, error in
      if let error = error as NSError? {
        fatalError("Unresolved error \(error), \(error.userInfo)")
      }
    }
    
    persistentContainer = container
  }
}
