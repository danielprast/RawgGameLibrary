//
//  GameLocalDataSourceTests.swift
//  Created by Daniel Prastiwa on 10/08/23.
//

import XCTest
import Combine
import CoreData
@testable import iOSAppSource


final class GameLocalDataSourceTests: XCTestCase {
  
  var context: NSManagedObjectContext!
  
  var sut: GameLocalDataSource!
  var cancellable = Set<AnyCancellable>()
  
  override func setUp() {
    let coreDataStack = CoreDataStack()
    context = coreDataStack.persistentContainer.viewContext
    sut = GameCoreDataLocalSource(moc: context)
  }
  
  func testAddToFavoriteSuccessfully() {
    // given
    let game = GameEntity.GameData(
      id: 3498,
      title: "Grand Theft Auto V",
      image: "https://media.rawg.io/media/games/456/456dea5e1c7e3cd07060c14e96612001.jpg",
      rating: 5,
      date: "2013-09-17"
    )
    let expectation = XCTestExpectation(description: "Adding game to favorite")
    var error: NError?
    var completionTask: Bool = false
    var success: Bool = false
    
    // when
    sut.add(favoriteGame: game)
      .subscribe(on: DispatchQueue.global(qos: .userInitiated))
      .eraseToAnyPublisher()
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()
      .sink { completion in
        switch completion {
        case .failure(let err):
          error = err
        case .finished:
          completionTask = true
        }
        expectation.fulfill()
      } receiveValue: { value in
        success = value
      }
      .store(in: &cancellable)
    
    // then
    wait(for: [expectation], timeout: 5)
    XCTAssertNil(error)
    XCTAssertTrue(completionTask)
    XCTAssertTrue(success)
  }
  
//  override func tearDown() {
//    <#code#>
//  }
  
}
