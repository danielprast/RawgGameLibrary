//
//  JsonLoader.swift
//  Created by Daniel Prastiwa on 10/08/23.
//

import Foundation
import XCTest
@testable import iOSAppSource


struct LocalJsonFileReader {
  
  static func readJsonFile(fileName: String) -> Data {
    guard let pathString = Bundle.module.path(
      forResource: fileName,
      ofType: "json"
    ) else {
      fatalError("\(fileName) not found")
    }
    
    guard let jsonString = try? String(contentsOfFile: pathString, encoding: .utf8) else {
      fatalError("Unable to convert \(fileName) to String")
    }
    
    print("🌁 json String: \(jsonString)")
    
    guard let jsonData = jsonString.data(using: .utf8) else {
      fatalError("Unable to convert \(fileName) to Data")
    }
    
    return jsonData
  }
  
}


extension XCTestCase {
  func readJsonFile(fileName: String) -> Data {
    guard let pathString = Bundle.module.path(
      forResource: fileName,
      ofType: "json"
    ) else {
      fatalError("\(fileName) not found")
    }
    
    guard let jsonString = try? String(contentsOfFile: pathString, encoding: .utf8) else {
      fatalError("Unable to convert \(fileName) to String")
    }
    
    print("🌁 json String: \(jsonString)")
    
    guard let jsonData = jsonString.data(using: .utf8) else {
      fatalError("Unable to convert \(fileName) to Data")
    }
    
    return jsonData
  }
  
  func decodeJsonData<T: Codable>(_ objectType: T.Type, data: Data) -> T? {
    do {
      let result = try JSONDecoder().decode(T.self, from: data)
      return result
    } catch let decodingError {
      print("Error: \(decodingError)")
      return nil
    }
  }
  
  func createSampleGameCollectionPage1() -> GameResponseModel {
    let jsonData = readJsonFile(fileName: "game_collection_page1")
    return decodeJsonData(GameResponseModel.self, data: jsonData)!
  }
}


struct ModuleLocalService {
  
  func parsingJSON<T>(
    of type: T.Type,
    from data: Data
  ) -> Result<T, NError> where T : Decodable, T : Encodable {
    do {
      let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
      
      let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
      let model = try JSONDecoder().decode(T.self, from: jsonData)
      return .success(model)
      
    } catch let error {
      print("parse_error", error)
    }
    
    return .failure(NError.custom("Parse Error"))
  }
  
  public func loadJsonFromFile(with url: String) -> Data {
    guard let pathString = Bundle.module.url(forResource: url, withExtension: "json") else {
      fatalError("File not found")
    }
    
    guard let data = try? Data(contentsOf: pathString) else {
      fatalError("Unable to convert json to String")
    }
    
    return data
  }
}
